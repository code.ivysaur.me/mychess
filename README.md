# mychess

![](https://img.shields.io/badge/written%20in-PHP%2C%20Javascript-blue)

A two-human chess service with RSS support.

You can monitor your games by RSS or email.

Tags: game


## Download

- [⬇️ myChess-1.1.7z](dist-archive/myChess-1.1.7z) *(20.90 KiB)*
- [⬇️ myChess-1.0.7z](dist-archive/myChess-1.0.7z) *(11.49 KiB)*
